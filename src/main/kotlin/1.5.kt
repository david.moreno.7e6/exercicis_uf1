/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/09/19
* TITLE: Operació boja
*/
import java.util.*
fun main() {
    val scanner1 = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introdueix un número:")
    val numero1 = scanner1.nextDouble()
    println("Aquest és el número introduït: ")
    println(numero1)
    val scanner2 = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introdueix un número:")
    val numero2 = scanner2.nextDouble()
    println("Aquest és el número introduït: ")
    println(numero2)
    val scanner3 = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introdueix un número:")
    val numero3 = scanner3.nextDouble()
    println("Aquest és el número introduït: ")
    println(numero3)
    val scanner4 = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introdueix un número:")
    val numero4 = scanner4.nextDouble()
    println("Aquest és el número introduït: ")
    println(numero4)
    val totalValue = (numero1 + numero2)*(numero3 % numero4)
    println("Aquest es el resultat: ")
    println(totalValue)
}