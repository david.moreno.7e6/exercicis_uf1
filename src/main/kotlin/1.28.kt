/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/09/19
* TITLE: Un és 10
*/
import java.util.*
fun main() {
    val scanner1 = Scanner(System.`in`)
    println("Introdueix un caràcter:")
    val numero1 = scanner1.nextInt()
    val scanner2 = Scanner(System.`in`)
    println("Introdueix un caràcter:")
    val numero2 = scanner2.nextInt()
    val scanner3 = Scanner(System.`in`)
    println("Introdueix un caràcter:")
    val numero3 = scanner3.nextInt()
    val scanner4 = Scanner(System.`in`)
    println("Introdueix un caràcter:")
    val numero4 = scanner4.nextInt()
    println((numero1==10) || (numero2==10) || (numero3==10) || (numero4==10) )
}