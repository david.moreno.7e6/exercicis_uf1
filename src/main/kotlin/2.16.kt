/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/10/07
* TITLE: Quants dies té el mes
*/
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    val mes = scanner.nextInt()
    when (mes){
        1 -> println("31")
        2 -> println("28")
        3 -> println("31")
        4 -> println("30")
        5 -> println("31")
        6 -> println("30")
        7 -> println("31")
        8 -> println("31")
        9 -> println("30")
        10 -> println("31")
        11 -> println("30")
        12 -> println("31")
    }
}
/* He tenido en cuenta que 1 es igual a enero y asi, con el ejercicio anterior le doy el valor de dias */