/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/10/16
* TITLE:Eleva’l
*/
import java.util.*
fun main() {
    val sc= Scanner(System.`in`)
    val n= sc.nextInt()
    var aux=1
    for (i in 1..n) {
        aux *= i
    }
    println(aux)
}