import java.util.Scanner

/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/10/11
* TITLE:Pinta X números
*/
fun main (){
    val sc= Scanner(System.`in`)
    val numero= sc.nextInt()

    for(i in 1..numero){
        println(i)
    }

}