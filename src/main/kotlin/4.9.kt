/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/10/20
* TITLE: Valors repetits
*/
fun main() {
    var list = arrayOf(1, 7, 3, 2, 4, 7, 5, 8, 7)
    var z=0
    for (x in list) {
        for(i in list) {
            if (x==i ){
                z++
            }
        }
        if(z>=2) {
            print(x)
            break
        }
        else if(z<2){
            z=0
        }
    }
}