/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/09/19
* TITLE: Calcula el descompte
*/
import java.util.*
fun main() {
    val scanner1 = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introdueix un número:")
    val preu_original = scanner1.nextDouble()
    println("Aquest és el número introduït: ")
    println(preu_original)
    val scanner2 = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introdueix un número:")
    val preu_actual = scanner2.nextDouble()
    println("Aquest és el número introduït: ")
    println(preu_actual)
    val total = ((preu_original-preu_actual)/preu_original)*100
    println("El resultat es: ")
    println(total)
}