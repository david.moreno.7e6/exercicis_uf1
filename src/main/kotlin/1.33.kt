/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/09/27
* TITLE: És divisible
*/
import java.util.*
fun main() {
    val scanner1 = Scanner(System.`in`)
    println("Introdueix un caràcter:")
    val numero = scanner1.nextInt()
    val scanner2 = Scanner(System.`in`)
    println("Introdueix un caràcter:")
    val numero2 = scanner2.nextInt()
    println(numero2%numero==0)

}