/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/09/19
* TITLE: Número següent
*/
import java.util.*
fun main() {
    val scanner1 = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introdueix un número:")
    val numero = scanner1.nextInt()
    println("Aquest és el número introduït: ")
    println(numero)
    val total= numero + 1
    println("Despres ve el: ")
    println(total)
}
