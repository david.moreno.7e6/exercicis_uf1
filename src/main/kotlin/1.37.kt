/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/09/27
* TITLE: Ajuda per la màquina de viatge en el temps
*/
import java.time.LocalDate
fun main() {
    val fecha= LocalDate.now()
    print("Avui és $fecha")
}