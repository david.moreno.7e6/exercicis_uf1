/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/09/23
* TITLE: Quant de temps?
*/
import java.util.*
fun main() {
    val scanner1 = Scanner(System.`in`)
    println("Introdueix un caràcter:")
    val segons = scanner1.nextInt()
    val hores= segons/3600
    val minuts= (segons-hores*3600)/60
    val segonsFinal= (segons-(hores*3600+minuts*60))
    println(hores)
    println("hora")
    println(minuts)
    println("minuts")
    println(segonsFinal)
    println("segons")
}