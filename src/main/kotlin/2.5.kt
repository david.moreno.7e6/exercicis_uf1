/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/09/29
* TITLE: En rang
*/
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un caràcter:")
    val n1 = scanner.nextInt()
    val scanner2 = Scanner(System.`in`)
    println("Introdueix un caràcter:")
    val n2 = scanner2.nextInt()
    val scanner3 = Scanner(System.`in`)
    println("Introdueix un caràcter:")
    val n3 = scanner3.nextInt()
    val scanner4 = Scanner(System.`in`)
    println("Introdueix un caràcter:")
    val n4 = scanner4.nextInt()
    val scanner5 = Scanner(System.`in`)
    println("Introdueix un caràcter:")
    val n5 = scanner5.nextInt()
    if (n5 in n1..n2 && n5 in n3..n4) {
        println("true")
    }
    else println("false")
}