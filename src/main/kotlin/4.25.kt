/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/11/30
* TITLE:Inverteix les paraules (3)
*/
import java.util.*
fun main () {
    val sc = Scanner(System.`in`)
    val secuenciaParaula = sc.nextLine()
    val list= secuenciaParaula.split(" ").reversed()
    for (i in 0..list.lastIndex){
        print(list[i].reversed()); print(" ")
    }
}