/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/09/19
* TITLE: És una lletra?
*/
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un caràcter:")
    val caracter1= scanner.next().single()
    println((caracter1.code >= 65 && caracter1.code <= 90) || (caracter1.code >=97 && caracter1.code <=122))
}

