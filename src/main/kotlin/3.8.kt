/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/10/12
* TITLE:Nombre de dígits
*/
import java.util.*
fun main() {
    val sc = Scanner(System.`in`)
    var numero = sc.nextInt()
    var digits=1
    while (numero/10!=0){
        numero/=10; digits++
    }
    print(digits)
}