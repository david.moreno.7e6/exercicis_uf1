/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/09/19
* TITLE: Transforma l'enter
*/
import java.util.*
fun main() {
    val scanner1 = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introdueix un número:")
    val numero = scanner1.nextDouble()
    println("Aquest és el número introduit: ")
    println(numero)
}