/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/09/19
* TITLE: Hello World!
*/
fun main(args: Array<String>) {
    println("Hello World!")
}