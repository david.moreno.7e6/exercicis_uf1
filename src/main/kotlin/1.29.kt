/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/09/19
* TITLE: Equacions de segon grau
*/
import java.util.*
import kotlin.math.sqrt
fun main() {
    val scanner1 = Scanner(System.`in`)
    println("Introdueix un caràcter:")
    val a = scanner1.nextInt()
    val scanner2 = Scanner(System.`in`)
    println("Introdueix un caràcter:")
    val b = scanner2.nextInt()
    val scanner3 = Scanner(System.`in`)
    println("Introdueix un caràcter:")
    val c = scanner3.nextInt()
    println((-b+sqrt((b*b).toDouble()-(4*a*c)))/(2*a))
    println((-b-sqrt((b*b).toDouble()-(4*a*c)))/(2*a))
}

