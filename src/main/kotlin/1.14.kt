/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/09/19
* TITLE: Divisor de compte
*/
import java.util.*
fun main() {
    val scanner1 = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introdueix un número:")
    val Comensals = scanner1.nextInt()
    println("Aquest és el número de comensals: ")
    println(Comensals)
    val scanner2 = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introdueix un número:")
    val Preu_sopar = scanner2.nextDouble()
    println("Aquest és el preu del sopar: ")
    println(Preu_sopar)
    val preuPerComençal = Preu_sopar / Comensals
    println("Aquest és el preu per comensal: ")
    print(preuPerComençal)
    print('€')
}