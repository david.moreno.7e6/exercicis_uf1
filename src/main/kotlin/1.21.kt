/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/09/19
* TITLE: Tres nombres iguals
*/
import java.util.*
fun main() {
    val scanner1 = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introdueix un número:")
    val numero1 = scanner1.nextInt()
    println("Aquest és el numero: ")
    println(numero1)
    val scanner2 = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introdueix un número:")
    val numero2 = scanner2.nextInt()
    println("Aquest és el numero: ")
    println(numero2)
    val scanner3 = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introdueix un número:")
    val numero3 = scanner3.nextInt()
    println("Aquest és el numero: ")
    println(numero3)
    println((numero1==numero2)&&(numero2==numero3))
}