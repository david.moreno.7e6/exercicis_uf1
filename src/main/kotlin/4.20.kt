/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/11/22
* TITLE:Distancia de hamming
*/

import java.util.*
fun main () {
    val sc = Scanner (System.`in`)
    val secuencia1= sc.next()
    val secuencia2= sc.next()
    val array1= mutableListOf<Char>()
    val array2= mutableListOf<Char>()
    var z=0
    var hamming=0
    for (x in secuencia1) {
        array1.add(x)
        z++
    }
    z=0
    for (x in secuencia2){
        array2.add(x)
        z++
    }

    for( x in 0..array1.lastIndex){
        if (array1[x]!=array2[x]){
            hamming++
        }
    }
    print(hamming)
}