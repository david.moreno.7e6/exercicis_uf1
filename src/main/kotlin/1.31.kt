/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/09/27
* TITLE: De metre a peu
*/
import java.util.*
fun main() {
    val scanner1 = Scanner(System.`in`)
    println("Introdueix un caràcter:")
    val metres = scanner1.nextFloat()
    val polzades = 39.37
    var peus = (metres * polzades).toFloat()
    peus = (peus / 12).toFloat()
    println(peus)
}