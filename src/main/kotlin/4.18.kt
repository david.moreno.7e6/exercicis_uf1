/* AUTHOR: David Moreno Fernandez
* DATE: 2022/10/30
* TITLE: Purga de caràcters
*/
import java.util.*
fun main () {
    val sc = Scanner(System.`in`)
    var secuencia = sc.next()
    var char= 'a'
    var secuenciaFinal= secuencia
    while (char != '0'){
        char = sc.next().single()
        for (i in secuencia){
            if(char==i){
                secuenciaFinal= secuenciaFinal.replace(char.toString(), "")
            }
        }
    }
    print(secuenciaFinal)
}