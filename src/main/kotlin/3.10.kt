/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/10/14
* TITLE: Extrems
*/
import java.util.*
fun main() {
    var n = 1
    var aux = 0
    var aux2 = 0
    while (n != 0) {
        val sc = Scanner(System.`in`)
        n = sc.nextInt()
        if (n != 0 && n > aux) {
            aux = n
            continue
        } else if (n != 0 && n < aux) {
            aux2 = n
            continue
        } else print("$aux $aux2"); break

    }
}