/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/10/07
* TITLE: Puges o baixes?
*/
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    val n1 = scanner.nextInt()
    val n2 = scanner.nextInt()
    val n3 = scanner.nextInt()
    if (n1<n2 && n2<n3 ) {
        println("ascendent")
    }
    else if (n1>n2 &&n2>n3) {
        println("descendent")
    }
    else println("cap de les dues")
}