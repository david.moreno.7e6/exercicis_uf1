/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/10/07
* TITLE: Valor absolut
*/
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    val n1 = scanner.nextInt()
    if (n1<0) {
        println(n1*-1)
    }
    else (println(n1))
}