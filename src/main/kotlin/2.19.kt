/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/10/09
* TITLE: Quina nota he tret?
*/
import java.util.Scanner
fun main() {
    val sc = Scanner(System.`in`)
    val Nota = sc.nextDouble()
    when (Nota) {
        in 0.0..4.9 -> println("Insuficient")
        in 5.0..5.9-> println("Suficient")
        in 6.0..6.9-> println("Bé")
        in 7.0..8.9-> println("Notable")
        in 9.0..10.0->println("Excelent")
        else -> println("Nota invalida")

    }
}
