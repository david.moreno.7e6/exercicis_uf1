/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/09/19
* TITLE: Dobla l'enter
*/
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introdueix un número:")
    val userInputValue = scanner.nextDouble()
    println("Aquest és el número introduït: ")
    println(userInputValue)
    val totalValue= userInputValue*2
    println(totalValue)
}