/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/09/29
* TITLE: Afegeix un segon (2)
*/
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un caràcter:")
    var hora = scanner.nextInt()
    val scanner2 = Scanner(System.`in`)
    println("Introdueix un caràcter:")
    var minut = scanner2.nextInt()
    val scanner3 = Scanner(System.`in`)
    println("Introdueix un caràcter:")
    var segons = scanner3.nextInt()
    if (++segons ==60) {
        segons =0
        minut +=1
    }
    if (minut==60) {
        minut=0
        hora +=1
    }
    if (hora==24) {
        hora = 0
    }
    val segonstoprint= if (segons <10) "0$segons" else "$segons"
    val minuttoprint= if (minut < 10) "0$minut" else "$minut"
    val horatoprint= if (hora < 10) "0$hora" else "$hora"
    println("$horatoprint:$minuttoprint:$segonstoprint")
}