/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/10/20
* TITLE: Igual a l'últim
*/
fun main() {
    var list = arrayOf(1, 7, 3, 2, 4, 7, 5, 8, 7)
    list.reverse()
    var x= list[0] //Si quito reverse y le doy valor a x= list[list.lastIndex], tambien dariamos con el valor del ultimo
    var y=-1
    for (i in list){
        if (x ==i){
            y++
        }
    }
    print(y)
}