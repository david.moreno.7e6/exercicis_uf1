/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/09/19
* TITLE: Calcual l'àrea
*/
import java.util.*
fun main() {
    val scanner1 = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introdueix un número:")
    val amplada = scanner1.nextDouble()
    println("Aquest és el número introduït: ")
    println(amplada)
    val scanner2 = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introdueix un número:")
    val llargada = scanner2.nextDouble()
    println("Aquest és el número introduït: ")
    println(llargada)
    val totalValue = amplada * llargada
    println("Aquest es el resultat de l'àrea: ")
    println(totalValue)
}