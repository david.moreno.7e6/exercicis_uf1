/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/09/29
* TITLE: Parell o senar?
*/
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un caràcter:")
    val numero = scanner.nextInt()
    if (numero == (numero/2)*2) {
        println("parell")
    }
    else if(numero!=(numero/2)*2) {
        println("senar")
    }
}