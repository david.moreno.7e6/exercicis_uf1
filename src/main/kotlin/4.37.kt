import java.util.Scanner

/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/12/01
* TITLE:Espirals
* Només funciona amb alguns numeros (4,5,6)
*/
fun main() {
    val sc = Scanner(System.`in`)
    var numero = 1
    var aux = 0
    val listaEspirals: MutableList<MutableList<String>> = mutableListOf()
    while (numero != 0) {
        numero = sc.nextInt()
        for (i in 0 until numero) {
            val columna = mutableListOf<String>()
            for (j in 0 until numero) {
                if (i == numero - 1) {
                    columna.add("X")
                } else if (j == numero - 1) {
                    columna.add("X")
                } else if (i == numero - 2 && j != numero - 1) {
                    columna.add(".")
                } else if (i == 1 && j != 1) {
                    columna.add(".")
                } else if (i == 1 && j % 2 != 0) {
                    columna.add("X")
                } else if (i == 0 && j != 0) {
                    columna.add("X")
                } else if (i == 0 && j == 0) {
                    columna.add(".")
                } else if (i == numero / 2) {
                    if (j % 2 != 0 || j != numero - 2 && j != 0) {
                        columna.add("X")
                    } else columna.add(".")

                } else if (i == numero / 2 - 1) {
                    if (j % 2 != 0) {
                        columna.add("X")
                    }
                    else columna.add(".")
                }
            }
            println(columna)
            listaEspirals.add(columna)
        }
    }
}


