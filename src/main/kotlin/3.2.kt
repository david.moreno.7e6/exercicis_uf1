import java.util.*

/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/10/11
* TITLE:Calcula la suma dels N primers
*/
fun main (){
    val sc= Scanner(System.`in`)
    val numero= sc.nextInt()
    var resultat= 0
    for (i in 1..numero){
        resultat += i
    }
    println(resultat)
}