/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/09/19
* TITLE: Qui riu últim riu millor
*/
import java.util.*
fun main() {
    val scanner1 = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introdueix un número:")
    val numero1 = scanner1.nextInt()
    val scanner2 = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introdueix un número:")
    val numero2 = scanner2.nextInt()
    val scanner3 = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introdueix un número:")
    val numero3 = scanner3.nextInt()
    val scanner4 = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introdueix un número:")
    val numero4 = scanner4.nextInt()
    val scanner5 = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introdueix un número:")
    val numero5 = scanner5.nextInt()
   println((numero5>numero1)&&(numero5>numero2)&&(numero5>numero3)&&(numero5>numero4))
}