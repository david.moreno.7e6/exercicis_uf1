/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/10/07
* TITLE: Any de traspàs
*/
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    val any = scanner.nextInt()
    if (any%4==0 && any%100!=0 || any%4==0 && any%400==0){
    println("año bisiesto")
    }
    else println("año no bisiesto")
}