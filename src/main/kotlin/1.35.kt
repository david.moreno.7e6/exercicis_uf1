/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/09/27
* TITLE: Creador de targetes de treball
*/
import java.util.*
fun main() {
    val scanner1 = Scanner(System.`in`)
    println("Introdueix un caràcter:")
    val nom = scanner1.next()
    val scanner2 = Scanner(System.`in`)
    println("Introdueix un caràcter:")
    val cognom = scanner2.next()
    val scanner3 = Scanner(System.`in`)
    println("Introdueix un caràcter:")
    val Despatx = scanner3.nextInt()
    print("Empleada: ")
    print(nom)
    print(" ")
    print(cognom)
    print(" - Despatx: ")
    print(Despatx)
}
