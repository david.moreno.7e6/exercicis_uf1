/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/10/09
* TITLE:Eleva’l
*/
import java.util.*
fun main() {
    val sc = Scanner(System.`in`)
    val n = sc.nextInt()
    var div=1L
    for (i in 1..n)
        if (n%i ==0) {
            div *=i
        }
    print(div)
}