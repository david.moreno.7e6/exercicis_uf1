/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/10/09
* TITLE:Eleva’l
*/
import java.util.*
fun main() {
    val sc = Scanner(System.`in`)
    val n = sc.nextInt()
    var aux=0
    for (i in 1 until n){
        aux = aux*10+i
        println(aux)
    }
    print("$aux$n")
}