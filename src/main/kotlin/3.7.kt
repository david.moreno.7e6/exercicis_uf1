/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/10/12
* TITLE:Revers de l’enter
*/
import java.util.*
fun main() {
    val sc = Scanner(System.`in`)
    var numero = sc.nextInt()
    var aux=0
    var aux2=0
    while(numero%10!=0){
        aux = numero % 10
        aux2= aux*10 + aux2 *10
        numero /= 10
    }
    print(aux2/10)
}