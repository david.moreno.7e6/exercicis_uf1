/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/09/19
* TITLE: Suma de dos nombres enters
*/
import java.util.*
fun main() {
    val scanner1 = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introdueix un número:")
    val numero1 = scanner1.nextDouble()
    println("Aquest és el número introduït: ")
    println(numero1)
    val scanner2 = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introdueix un número:")
    val numero2 = scanner2.nextDouble()
    println("Aquest és el número introduït: ")
    println(numero2)
    val totalValue = numero1 + numero2
    println("Aquest es el resultat: ")
    println(totalValue)
}