/* AUTHOR: David Moreno Fernandez
* DATE: 2022/10/30
* TITLE: Substitueix el caràcter
*/
import java.util.*
fun main () {
    val sc = Scanner(System.`in`)
    var secuencia = sc.next()
    val caracterOld = sc.next().single()
    val caracterNew = sc.next().single()
    var secuenciaFinal= secuencia
    for (i in secuencia){
        if (i == caracterOld){
            secuenciaFinal= secuencia.replace(i, caracterNew)
        }
    }
    print(secuenciaFinal)
}