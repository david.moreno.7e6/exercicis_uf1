
/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/09/19
* TITLE: Nombres decimals iguals
*/
import java.util.*
fun main() {
    val scanner1 = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introdueix un número:")
    val numero1 = scanner1.nextDouble()
    println("Aquest és el numero: ")
    println(numero1)
    val scanner2 = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introdueix un número:")
    val numero2 = scanner2.nextDouble()
    println("Aquest és el numero: ")
    println(numero2)
    println(numero1==numero2)
}