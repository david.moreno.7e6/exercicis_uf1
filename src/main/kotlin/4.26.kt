/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/11/22
* TITLE:Hola i adeu
*/

import java.util.*
fun main () {
    val sc = Scanner(System.`in`)
    val paraula = sc.next()

    if ("hola" in paraula|| "HOLA" in paraula) {
        print("Hola")
    }
    else print("adeu")
}