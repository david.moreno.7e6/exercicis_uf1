/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/10/07
* TITLE: És vocal o consonant?
*/
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    var letra = scanner.next().single()
    val valorletra =letra.toInt()
    if (valorletra == 65 || valorletra == 69 ||valorletra == 73 ||valorletra == 79 ||valorletra == 85 ||valorletra == 97 ||valorletra == 101 ||valorletra == 105 ||valorletra == 111 || valorletra == 117 ){
        println("vocal") }
    else println("consonant")

}