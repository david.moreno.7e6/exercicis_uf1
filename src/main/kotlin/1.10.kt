/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/09/19
* TITLE: Quina es la mida de la meva pizza?
*/
import java.util.*
fun main() {
    val scanner1 = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introdueix un número:")
    val diametro = scanner1.nextDouble()
    println("Aquest és el número introduït: ")
    println(diametro)
    val total = Math.PI * ((diametro * diametro) / 4)
    println(total)
}