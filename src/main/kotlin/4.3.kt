/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/10/20
* TITLE: Calcula la lletra del dni
*/
import java.util.*
fun main (args: Array<String>) {
    val letra= arrayOf("T","R","W","A","G","M","Y","F","P","D","X","B","N","J","Z","S","Q","V","H","L","C","K","E")
    val sc= Scanner(System.`in`)
    val numDNI= sc.nextInt()
    val div= numDNI%23
    print(numDNI)
    print(letra[div])
}