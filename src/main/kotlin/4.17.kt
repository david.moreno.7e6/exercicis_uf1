/* AUTHOR: David Moreno Fernandez
* DATE: 2022/10/30
* TITLE: Són iguals? (2)
*/
import java.util.*
fun main (){
    val sc= Scanner(System.`in`)
    val secuenciaPrimera= sc.next().uppercase()
    val secuenciaSegona= sc.next().uppercase()
    if (secuenciaPrimera==secuenciaSegona){
        print("Son iguals")
    }
    else print("No son iguals")
}