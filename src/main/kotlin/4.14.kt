import java.util.Scanner

/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/10/20
* TITLE: Quants sumen...?
*/
fun main() {
    var list = arrayOf(25, 5, 2, 10, 33, 15, 40, 1, 20, 4, 11)
    val sc = Scanner(System.`in`)
    val numero = sc.nextInt()
    for (x in list){
        for (i in list){
            if (x + i == numero) {
                println("$x $i")
            }
        }
    }
}