import java.util.*

/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/10/11
* TITLE:Imprimeix el rang (2)
*/

fun main() {
    val sc = Scanner(System.`in`)
    val numero = sc.nextInt()
    val numeroF=sc.nextInt()
    if (numeroF < numero){
        for (i in numero downTo numeroF+1){
            print("$i,")
        }
        print(numeroF)
    }
    else if (numero < numeroF){
        for(i in numero until numeroF ){
            print("$i,")
        }
        print(numeroF)
    }
    else print(numero)
}