/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/10/19
* TITLE: Rombe d’*
*/
import java.util.*
fun main() {
    val sc = Scanner(System.`in`)
    var ninit = sc.nextInt()
    var n = 2*ninit -1
    var aux =0
    var aux2=0
    for (i in 1..n step 2) {
        aux= (n-i)/2
        aux2= n-aux*2
        repeat(aux){
            print(" ")
        }
        repeat(aux2){
            print("*")
        }
        repeat(aux){
            print("")
        }
        println()
    }
    for ( i in 1 until n step 2) {
        aux++
        aux2= n-aux*2
        repeat (aux) {
            print(" ")
        }
        repeat (aux2) {
            print("*")
        }
        repeat (aux){
            print(" ")
        }
        println()
    }
}