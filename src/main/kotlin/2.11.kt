/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/10/06
* TITLE: Calculadora
*/
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un caràcter:")
    var n1 = scanner.nextInt()
    val scanner2 = Scanner(System.`in`)
    println("Introdueix un caràcter:")
    var n2 = scanner2.nextInt()
    val scanner3 = Scanner(System.`in`)
    println("Introdueix un caràcter:")
    val simbol = scanner3.next().single()
    if (simbol.hashCode()==37) {
        println(n1%n2)
    }
    else if(simbol.hashCode()==42) {
        println(n1*n2)
    }
    else if(simbol.hashCode()==43) {
        println(n1+n2)
    }
    else if(simbol.hashCode()==45) {
        println(n1-n2)
    }
    else if(simbol.hashCode()==47) {
        println(n1/n2)
    }
    else println("ERROR")

}