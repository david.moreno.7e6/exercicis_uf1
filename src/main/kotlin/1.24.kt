/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/09/19
* TITLE: És un nombre?
*/
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un caràcter:")
    val caracter1= scanner.next().single()
    println((caracter1.code >= 48 && caracter1.code <= 57))
}