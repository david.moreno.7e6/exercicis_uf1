/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/09/29
* TITLE: És un bitllet vàlid
*/
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un caràcter:")
    val numero = scanner.nextInt()
    if (numero==5 || numero==10 || numero==20 || numero==50 || numero== 100 || numero==200 || numero==500) {
        println("true")
    }
    else println("false")
}