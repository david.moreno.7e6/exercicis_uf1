import java.util.Scanner

/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/11/30
* TITLE:Files i columnes
*/
fun main() {
    val sc = Scanner(System.`in`)
    var filas = sc.nextInt()
    val columnas = sc.nextInt()
    val matriu: MutableList<MutableList<Int>> = mutableListOf()

    for (i in 0 until filas) {
        val valorsColumnas = mutableListOf<Int>()
        for (j in 0 until columnas) {
            val random = (1..9).random()
            valorsColumnas.add(random)
        }
        matriu.add(valorsColumnas)
    }
    for (i in 0..matriu.lastIndex) {
        println(matriu[i])
    }
    val valor= sc.next()
    when (valor){
        "fila"-> {
            val numFila= sc.nextInt()
            print("Fila $numFila : ")
            print(matriu[numFila])
        }
        "columna"-> {
            val numColumna= sc.nextInt()
            print("columna $numColumna : ")
            for (i in 0 until columnas){

                print("${matriu[i][numColumna]} ")
            }
        }
        "element"-> {
            val numFila = sc.nextInt()
            val numColumna= sc.nextInt()
            print("element $numFila $numColumna : ")
            print(matriu[numFila][numColumna])
        }
    }

}
    //PROVES MATRIUS
//    PRINT MATRIU
//    for (i in 0..matriu.lastIndex){
//        println(matriu[i])
//    }
//    MATRIU DIAGONAL
//    val matriuDiagonal= mutableListOf<Int>()
//    for (j in filas-1 downTo 0){
//        filas--
//        matriuDiagonal.add(matriu[filas][j])
//    }
//    println(matriuDiagonal)




