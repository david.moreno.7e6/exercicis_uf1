/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/09/19
* TITLE: És edat legal
*/
import java.util.*
fun main() {
    val scanner1 = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introdueix un número:")
    val edat = scanner1.nextInt() >=18
    println("Aquest és el número introduit: ")
    println(edat)
}
