/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/09/29
* TITLE: Canvi mínim
*/
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un caràcter:")
    val euros= scanner.nextInt()
    val scanner2= Scanner(System.`in`)
    println("Introdueix un caràcter:")
    val centims= scanner2.nextInt()
    val b500= 500
    val b200= 200
    val b100= 100
    val b50= 50
    val b20= 20
    val b10= 10
    val b5= 5
    val m2= 2
    val m1= 1
    val c50= 50
    val c20= 20
    val c10= 10
    val c5= 5
    val c2= 2
    val c1= 1
    if (euros/b500 >=1) {
        val b500= euros/b500
        println("Bitllets de 500:$b500")
    }
    else println("Billets de 500: 0")
    if ((euros%b500)/b200 >=1) {
       val b200= (euros%b500)/b200
        println("Bitllets de 200: $b200")
    }
    else println("Billets de 200: 0")
    if (((euros%b500)%b200)/b100 >=1) {
        val b100=((euros%b500)%b200)/b100
        println("Bitllets de 100: $b100")
    }
    else println("Billets de 100: 0")
    if ((((euros%b500)%b200)%b100)/b50 >=1) {
        val b50= (((euros%b500)%b200)%b100)/b50
        println("Bitllets de 50: $b50")
    }
    else println("Billets de 50: 0")
    if (((((euros%b500)%b200)%b100)%b50)/b20 >=1) {
        val b20=((((euros%b500)%b200)%b100)%b50)/b20
        println("Bitllets de 20: $b20")
    }
    else println("Billets de 20: 0")
    if (((((((euros%b500)%b200)%b100)%b50)%b20)/b10) >=1) {
        val b10=(((((euros%b500)%b200)%b100)%b50)%b20)/b10
        println("Bitllets de 10: $b10")
    }
    else println("Billets de 10: 0")
    if (((((((euros%b500)%b200)%b100)%b50)%b20)%b10)/b5 >=1) {
        val b5=((((((euros%b500)%b200)%b100)%b50)%b20)%b10)/b5
        println("Bitllets de 5: $b5")
    }
    else println("Billets de 5: 0")
    if ((((((((euros%b500)%b200)%b100)%b50)%b20)%b10)%b5)/m2 >=1) {
        val m2=(((((((euros%b500)%b200)%b100)%b50)%b20)%b10)%b5)/m2
        println("Monedes de 2: $m2")
    }
    else println("Monedes de 2: 0")
    if (((((((((euros%b500)%b200)%b100)%b50)%b20)%b10)%b5)%m2)/m1 >=1) {
        val m1=((((((((euros%b500)%b200)%b100)%b50)%b20)%b10)%b5)%m2)/m1
        println("Monedes de 1: $m1")
    }
    else println("Monedes de 1: 0")

    if (centims/c50 >=1) {
        val c50= centims/c50
        println("Monedes de 50 centims: $c50")
    }
    else println("Monedes de 50 centims: 0")
    if ((centims%c50)/c20 >=1) {
        val c20= (centims%c50)/c20
        println("Monedes de 20 centims: $c20")
    }
    else println("Monedes de 20 centims: 0")
    if (((centims%c50)%c20)/c10 >=1) {
        val c10= ((centims%c50)%c20)/c10
        println("Monedes de 10 centims: $c10")
    }
    else println("Monedes de 10 centims: 0")
    if ((((centims%c50)%c20)%c10)/c5 >=1) {
        val c5= (((centims%c50)%c20)%c10)/c5
        println("Monedes de 5 centims: $c5")
    }
    else println("Monedes de 5 centims: 0")
    if (((((centims%c50)%c20)%c10)%c5)/c2 >=1) {
        val c2= ((((centims%c50)%c20)%c10)%c5)/c2
        println("Monedes de 2 centims: $c2")
    }
    else println("Monedes de 2 centims: 0")
    if ((((((centims%c50)%c20)%c10)%c5)%c2)/c1 >=1) {
        val c1= (((((centims%c50)%c20)%c10)%c5)%c2)%c1
        println("Monedes de 1 centim: $c1")
    }
    else println("Monedes de 1 centim: 0")
}