/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/09/19
* TITLE: De Celsius a Fahrenheit
*/
import java.util.*
fun main() {
    val scanner1 = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introdueix un número:")
    val Celsius = scanner1.nextDouble()
    println("Aquest és el número de graus Celsius: ")
    println(Celsius)
    val total = Celsius * 1.8 + 32
    println("Aquest és el número de graus Fahrenheit: ")
    println(total)
}