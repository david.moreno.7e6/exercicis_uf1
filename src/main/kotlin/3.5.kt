import java.util.*

/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/10/11
* TITLE:Taula de multiplicar
*/
fun main() {
    val sc = Scanner(System.`in`)
    val taula = sc.nextInt()
    var resultat= 1
    val x= "X"
    for (i in 1..10){
        resultat= taula * i
        println("$taula$x$i = $resultat")
    }
}