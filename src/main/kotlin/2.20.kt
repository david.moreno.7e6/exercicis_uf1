/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/10/09
* TITLE:Conversor d’unitats
*/
import java.util.*
fun main() {
    val sc = Scanner(System.`in`)
    val numero = sc.nextDouble()
    val sca = Scanner(System.`in`)
    val unitat = sca.nextLine()
    if (unitat== "G") {
        print(numero/1000)
        println(" KG")
        print(numero/1000000)
        println(" TN")
    }
    else if (unitat == "KG") {
        print(numero*1000);println(" G")
        print(numero/1000);print("TN")
    }
    else if (unitat == "TN") {
        print(numero*1000000);println(" G")
        print(numero*1000);println(" KG")
    }
}