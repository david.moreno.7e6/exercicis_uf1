/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/11/30
* TITLE:Inverteix les paraules
*/
import java.util.*
fun main () {
    val sc = Scanner(System.`in`)
    val secuenciaParaula = sc.nextLine()
    val list= secuenciaParaula.split(" ")
    for (i in 0..list.lastIndex){
        println(list[i].reversed())
    }
}