/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/10/19
* TITLE: Triangle invertit d’*
*/
import java.util.*
fun main() {
    val sc = Scanner(System.`in`)
    val n = sc.nextInt()
    for (i in 0..n) {
        var aux = n - i
        repeat (i){
        print(" ") }
        repeat (aux) {
            print("*")
        }
        println()
    }
}

