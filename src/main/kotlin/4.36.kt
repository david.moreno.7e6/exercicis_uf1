import java.util.Scanner

/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/12/01
* TITLE:Dominó
*/
fun main (){
    val sc= Scanner(System.`in`)
    val numeroPartidas= sc.nextInt()
    val paraulesAnna= mutableListOf<String>()
    val paraulesBernat= mutableListOf<String>()
    var winner= false
    for (i in 1..numeroPartidas) {
        println("Partida numero $i")
        winner= false
        for (j in 1..5) {
            println("Paraula $j Anna")
            val paraulaAnna = sc.next()
            println("Paraula $j Bernat")
            val paraulaBernat = sc.next()
            paraulesAnna.add(paraulaAnna)
            paraulesBernat.add(paraulaBernat)
        }
        for (l in 0 until 5){
            if (paraulesAnna[l][paraulesAnna[l].lastIndex] != paraulesBernat[l][0]){
                println("A")
                winner=true
                break
            }
            else if (paraulesBernat[l][paraulesBernat[l].lastIndex] != paraulesAnna[l][0]){
                println("B")
                winner= true
                break
            }
        }
        if (winner== false){
            println("=")
        }
    }
}