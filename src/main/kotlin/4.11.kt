/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/10/20
* TITLE: Quants parells i quants senars?
*/
fun main() {
    var list = arrayOf(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11)
    var parells=0
    var senars=0
    for (x in list){
        if (x%2==0) {
            parells++
        }
        else { senars++}
    }
    println("Parells: $parells"); print("Senars: $senars")
}