/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/09/19
* TITLE: Afegeix un segon
*/
import java.util.*
fun main() {
    val scanner1 = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introdueix un número:")
    val segons = scanner1.nextInt()
    println("Aquest és el número de segons: ")
    println(segons)
    println((segons+1)%60)
}