/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/09/19
* TITLE: Quina temperatura fa?
*/
import java.util.*
fun main() {
    val scanner1 = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introdueix un número:")
    val Temperatura = scanner1.nextDouble()
    println("Aquesta es la temperatura: ")
    println(Temperatura)
    val scanner2 = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introdueix un número:")
    val augment = scanner2.nextDouble()
    println("Aquest és l'augment: ")
    println(augment)
    val total = Temperatura + augment
    println("La temperatura actual es: ")
    print(total)
    print('º')
}