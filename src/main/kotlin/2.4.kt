/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/09/29
* TITLE: Té edat per treballar
*/
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un caràcter:")
    val edat = scanner.nextInt()
    if (edat >=16 && edat <=65) {
        println("Té edat per treballar")
    }
    else println("No té edat per treballar")
}