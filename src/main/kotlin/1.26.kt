/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/09/19
* TITLE: Fes-me minúscula
*/
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un caràcter:")
    val caracter1= scanner.next().single()
    val test=('a'-'A')
    val test2=(caracter1.code >=65 && caracter1.code <=90)
    println(caracter1+test)
}
