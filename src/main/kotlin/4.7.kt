/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/10/20
* TITLE: Inverteix l’array
*/
fun main() {
    var list = arrayOf(1, 2, 3, 4, 5, 6, 7, 8, 9, 0)
//    list.reverse()
//    print(list.contentToString())
    val inverted= arrayOf(1, 2, 3, 4, 5, 6, 7, 8, 9, 0)
    for (i in 0..list.lastIndex){
        inverted[i]= list[list.lastIndex-i]
    }
    print(inverted.contentToString())
}