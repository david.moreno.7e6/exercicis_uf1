/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/10/06
* TITLE: Calcula la lletra del dni
*/
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un caràcter:")
    val nDNI= scanner.nextInt()
    val DNI= nDNI%23
    if( DNI==0) {
        val letra="T"
        println("$nDNI$letra")
    }
    else if(DNI==1) {
        val letra="R"
        println("$nDNI$letra")
    }
    else if(DNI==2) {
        val letra="W"
        println("$nDNI$letra")
    }
    else if(DNI==3) {
        val letra="A"
        println("$nDNI$letra")
    }
    else if(DNI==4) {
        val letra="G"
        println("$nDNI$letra")
    }
    else if(DNI==5) {
        val letra="M"
        println("$nDNI$letra")
    }
    else if(DNI==6) {
        val letra="Y"
        println("$nDNI$letra")
    }
    else if(DNI==7) {
        val letra="F"
        println("$nDNI$letra")
    }
    else if(DNI==8) {
        val letra="P"
        println("$nDNI$letra")
    }
    else if(DNI==9) {
        val letra="D"
        println("$nDNI$letra")
    }
    else if(DNI==10) {
        val letra="X"
        println("$nDNI$letra")
    }
    else if(DNI==11) {
        val letra="B"
        println("$nDNI$letra")
    }
    else if(DNI==12) {
        val letra="N"
        println("$nDNI$letra")
    }
    else if(DNI==13) {
        val letra="J"
        println("$nDNI$letra")
    }
    else if(DNI==14) {
        val letra="Z"
        println("$nDNI$letra")
    }
    else if(DNI==15) {
        val letra="S"
        println("$nDNI$letra")
    }
    else if(DNI==16) {
        val letra="Q"
        println("$nDNI$letra")
    }
    else if(DNI==17) {
        val letra="V"
        println("$nDNI$letra")
    }
    else if(DNI==18) {
        val letra="H"
        println("$nDNI$letra")
    }
    else if(DNI==19) {
        val letra="L"
        println("$nDNI$letra")
    }
    else if(DNI==20) {
        val letra="C"
        println("$nDNI$letra")
    }
    else if(DNI==21) {
        val letra="K"
        println("$nDNI$letra")
    }
    else if(DNI==22) {
        val letra="E"
        println("$nDNI$letra")
    }
    else println("ERROR")
}