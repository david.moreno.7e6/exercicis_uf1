/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/09/27
* TITLE: Calcula el capital (no LA capital)
*/
import java.util.*
fun main() {
    val scanner1 = Scanner(System.`in`)
    println("Introdueix un caràcter:")
    val capital = scanner1.nextDouble()
    val scanner2 = Scanner(System.`in`)
    println("Introdueix un caràcter:")
    val anys = scanner2.nextDouble()
    val scanner3 = Scanner(System.`in`)
    println("Introdueix un caràcter:")
    val interes = (scanner3.nextDouble())/100
    val final= (capital*(anys*interes))+capital
    println(final)
}