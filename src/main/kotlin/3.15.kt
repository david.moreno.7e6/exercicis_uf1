/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/10/14
* TITLE:Endevina el número (ara amb intents)
*/
import java.util.*
fun main() {
    var numero=0
    var i=1
    val numero_secreto = (1..100).random()
    var intents=0
    while (i in (1..100)) {
        val sc = Scanner(System.`in`)
        var numero = sc.nextInt();
        intents++
        if (numero > numero_secreto && intents <6) {
            println("Massa alt"); println("numero d'intents: $intents")
            continue
        }
        else if (numero < numero_secreto && intents <6) {
            println("Massa baix"); println("numero d'intents: $intents")
            continue
        }
        else if (numero == numero_secreto && intents <6) {
            println("has acertat");
            println("numero d'intents: $intents")
            break
        }
        else println("Has perdut")

    }
}