/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/10/14
* TITLE: Suma de fraccions
*/
import java.util.*
fun main() {
    val sc = Scanner(System.`in`)
    val n = sc.nextInt()
    var aux =1.0
    var aux2=0.0
    for (i in 1..n) {
        aux = 1.0 / i
        aux2 += aux
    }
    print (aux2)
}