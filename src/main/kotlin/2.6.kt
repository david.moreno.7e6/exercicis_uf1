/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/09/29
* TITLE: Quina pizza és més gran?
*/
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un caràcter:")
    val diametre1 = scanner.nextInt()
    val scanner2 = Scanner(System.`in`)
    println("Introdueix un caràcter:")
    val costat1 = scanner2.nextInt()
    val scanner3 = Scanner(System.`in`)
    println("Introdueix un caràcter:")
    val costat2 = scanner3.nextInt()
    val areaPizza1=(3.1416*((diametre1/2)*(diametre1/2)))
    val areaPizza2= costat1*costat2
    if (areaPizza1>areaPizza2) {
        println("Pizza rodona:$areaPizza1")
    }
    else if (areaPizza2>areaPizza1) {
        println("Pizza rectangular:$areaPizza2")
    }

}