import java.util.Scanner

/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/10/20
* TITLE: La suma total
*/

fun main () {
    val secuencia = arrayOf(1, 1, 5, 3, 1, 0)
    val suma = 0
    var result = 0
    var calcul = 0
    var same = false
    for (i in 0..secuencia.lastIndex) {
        calcul = secuencia[i]
        result += calcul
    }
    for (i in secuencia){
        if (result-i==i){
            same=true
            print("Si")
        }
    }
    if (same == false){
        print("No")
    }
}