/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/10/20
* TITLE: Suma el valors
*/
fun main (args: Array<String>) {
    val x= arrayOf<Int>(-3,10,0,-2,8)
    var suma=0
    for (i in x) {
        suma += i
    }
    val red= "\u001b[31m"
    println(red+ suma)
}