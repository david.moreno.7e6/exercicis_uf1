import java.util.Scanner

/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/10/30
* TITLE: Són iguals?
*/
fun main (){
    val sc= Scanner(System.`in`)
    val secuenciaPrimera= sc.next()
    val secuenciaSegona= sc.next()

    if (secuenciaPrimera==secuenciaSegona){
        print("Son iguals")
    }
    else print("No son iguals")
}
