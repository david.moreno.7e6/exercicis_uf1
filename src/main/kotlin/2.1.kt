/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/09/29
* TITLE: Màxim de 3 nombres enters
*/

import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un caràcter:")
    val numero1= scanner.nextInt()
    val scanner2 = Scanner(System.`in`)
    println("Introdueix un caràcter:")
    val numero2= scanner2.nextInt()
    val scanner3 = Scanner(System.`in`)
    println("Introdueix un caràcter:")
    val numero3= scanner3.nextInt()
    if (numero1 >= numero2 && numero1>=numero3) {
        println(numero1)}
        else if(numero2>=numero1 && numero2>=numero3) {
            println(numero2) }
        else if(numero3>=numero1 && numero3>=numero2) {
            println(numero3)
    }
}