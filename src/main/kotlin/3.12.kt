import java.util.*

/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/10/14
* TITLE:Divisible per 3 i per 5
*/
fun main() {
    val sc = Scanner(System.`in`)
    val numero = sc.nextInt()
    for (i in 1..numero){
        if (i%3==0){
            println("$i es divisible per 3")
        }
        else if (i%5==0){
            println("$i es divisible per 5")
        }
    }
}