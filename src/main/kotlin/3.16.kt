/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/10/16
* TITLE:Coordenades en moviment
*/
import java.util.*
fun main() {
    var n = 0
    var s = 0
    var e = 0
    var o = 0
    var moviment:Char
    var i = 1
    while (i in 1..100) {
        val sc = Scanner(System.`in`)
        moviment = sc.next().single()
        if (moviment == 'n' && moviment != 'z') {
            n--; continue
        } else if (moviment == 's' && moviment != 'z') {
            s++;continue
        } else if (moviment == 'o' && moviment != 'z') {
            o--;continue
        } else if (moviment == 'e' && moviment != 'z') {
            e++;continue
        } else
            break
    }
    val posicioVertical = n + s
    val posicioHorizontal = e + o
    println("($posicioHorizontal,$posicioVertical)")

}