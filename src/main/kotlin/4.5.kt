/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/10/20
* TITLE: En quina posició?
*/
import java.util.*
fun main(){
    val list= arrayOf(1,2,8,9,45,47,12,46,35,0)
    val sc= Scanner(System. `in`)
    val num= sc.nextInt()
    var x=0
    for (i in list){
        x++
        if (num!=i && x==10){
            print("No està contingut")
        }
        else if (num!=i) {
            continue
        }
        else {
            print(x)
            break
        }
    }
}