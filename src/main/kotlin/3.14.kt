/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/10/14
* TITLE:Endevina el número
*/
import java.util.*
fun main() {
    var numero=0
    var i=1
    val numero_secreto = (1..100).random()
    while (i in (1..100)) {
        val sc = Scanner(System.`in`)
        var numero = sc.nextInt();
        if (numero > numero_secreto) {
            println("Massa alt")
            continue
        } else if (numero < numero_secreto) {
            println("Massa baix")
            continue
        } else println("has acertat")
        break
    }
}