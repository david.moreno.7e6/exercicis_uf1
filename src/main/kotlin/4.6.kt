/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/10/20
* TITLE: Mínim i màxim
*/
import java.util.*
fun main() {
    val list = arrayOf(2, 8, 19, -32, 189, -1, -68, 190, 0, 1)
    var min= 0
    var max= 0
    for (i in list){
        if (i>max) {
            max=i
        }
        else if (i < min){
            min=i
        }
    }
    println(max)
    print(min)
}