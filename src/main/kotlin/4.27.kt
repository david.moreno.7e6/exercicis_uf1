/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/11/22
* TITLE:Elimina les repeticions
*/

import java.util.*
fun main () {
    val sc = Scanner(System.`in`)
    val numero = sc.nextInt()
    val lista= mutableListOf<Int>()
    for (i in 1..numero ){
        val secuencia= sc.nextInt()
        if(secuencia !in lista){
            lista.add(secuencia)
        }
    }
    println(lista)

}