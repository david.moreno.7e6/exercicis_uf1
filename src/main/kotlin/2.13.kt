/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/10/07
* TITLE: Quin dia de la setmana?
*/
/*
a = (14 - Mes) / 12
y = Año - a
m = Mes + 12 * a - 2

Para el calendario Juliano:
d = (5 + dia + y + y/4 + (31*m)/12) mod 7

Para el calendario Gregoriano:
 d = (día + y + y/4 - y/100 + y/400 + (31*m)/12) mod 7
El resultado es un cero (0) para el domingo, 1 para el lunes… 6 para el sábado
*/


import java.util.*


fun main() {
    val scanner = Scanner(System.`in`)
    var dia = scanner.nextInt()
    val scanner2 = Scanner(System.`in`)
    val mes = scanner2.nextInt()
    val scanner3 = Scanner(System.`in`)
    val any = scanner3.nextInt()
    val a =(14-mes)/12
    val y= any-a
    val m= mes+12*a-2
    val ndiafinal=(dia+y+(y/4)-(y/100)+(y/400)+(31*m)/12) %7
    when (ndiafinal){
        0 -> print("Domingo")
        1 -> print("Lunes")
        2 -> print("Martes")
        3 -> print("Miercoles")
        4 -> print("Jueves")
        5 -> print("Viernes")
        6 -> print("Sabado")
}
}