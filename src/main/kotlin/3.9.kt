/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/10/12
* TITLE:És primer?
*/
import java.util.*
fun main() {
    val sc = Scanner(System.`in`)
    var numero = sc.nextInt()
    var numDivisibles=0
    for (i in 1..numero){
        if (numero%i==0){
            numDivisibles++
        }
    }
    if (numDivisibles==2){
        print("És primer")
    }
    else print("No és primer")
}
