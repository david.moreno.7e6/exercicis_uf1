/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/11/22
* TITLE:Paréntesis
*/
import java.util.*
fun main () {
    val sc = Scanner (System.`in`)
    val parentesis= sc.next()
    val cierre= ")"
    if (parentesis[parentesis.length-1].hashCode() == cierre.hashCode() ){
        println("Si")
    }
    else println("No")
}