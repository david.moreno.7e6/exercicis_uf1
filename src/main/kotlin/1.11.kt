/*
* AUTHOR: David Moreno Fernandez
* DATE: 2022/09/19
* TITLE: Calculadora de volum d'aire
*/
import java.util.*
fun main() {
    val scanner1 = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introdueix un número:")
    val llargada = scanner1.nextDouble()
    println("Aquest és el número introduït: ")
    println(llargada)
    val scanner2 = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introdueix un número:")
    val amplada = scanner2.nextDouble()
    println("Aquest és el número introduït: ")
    println(amplada)
    val scanner3 = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introdueix un número:")
    val alçada = scanner3.nextDouble()
    println("Aquest és el número introduït: ")
    println(alçada)
    val total = llargada * amplada * alçada
    println(total)
}